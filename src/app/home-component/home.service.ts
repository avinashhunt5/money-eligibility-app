import { catchError, retry } from 'rxjs/internal/operators';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';

@Injectable({
    providedIn: 'root'
  })
export class HomeService {

    constructor(private httpClient: HttpClient) {}


     checkEligibility(request: Object): Observable<any> {
        let requestUrl = 'http://localhost:8083/money-eligibility-service/eligibility/isEligible';
        return this.httpClient.post(requestUrl,request).pipe(retry(3), 
                 catchError(this.handleError<any>('checkEligibility')));
      }

     private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
          console.error(error);
          this.log(`${operation} failed: ${error.message}`);
          return of(result as T);
        };
      }
      
      private log(message: string) {
        console.log(message);
      }
  
}