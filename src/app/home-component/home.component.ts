import { Component, AfterViewInit } from '@angular/core';
import { Options, LabelType } from 'ng5-slider';
import { HomeService } from './home.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements AfterViewInit {
  
  filters: any;
  responseMsg: string;
  isEligible: boolean;
  isEligibleResponse: boolean;

  salary = {
    value: "25"
  }
  gold = {
    value: "25"
  }

  query = {
    salary: "",
    gold: ""
  }
  yrToggel: boolean;

  soptions: Options = {
    floor: 1,
    ceil: 25,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          return value + '<b>L</b>';
        case LabelType.High:
          return value + '<b>L</b>';
        default:
          return value + '<b>L</b>';
      }
    }
  };

  goptions: Options = {
    floor: 1,
    ceil: 25,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          return value + '<b>L</b>';
        case LabelType.High:
          return value + '<b>L</b>';
        default:
          return value + '<b>L</b>';
      }
    }
  };
 
  constructor(private homeService: HomeService) {
    this.isEligibleResponse = false;
    this.yrToggel = true;
  }

  ngAfterViewInit() {
    this.isEligibleResponse = false;
    this.update();
  }

  tbupdate(id) {
    if (id == 0) {
      this.salary.value = (Number(this.query.salary)).toString();
    }
    else if (id == 1) {
      this.gold.value = (Number(this.query.gold)).toString();
    }
    this.update();
  }

  update() {
    var salaryAmount = Number(this.salary.value) * 100000;
    this.query.salary = salaryAmount.toString();
    var goldAmount = Number(this.gold.value) * 100000;
    this.query.gold = goldAmount.toString();
  }
  
  checkEligibility() {
    let request = {
        "salary" : this.query.salary,
        "goldAsset" : this.query.gold
    };
    this.homeService.checkEligibility(request)
           .subscribe(data => {
                this.responseMsg = data.message;
                this.isEligible = data.eligible;
                this.isEligibleResponse = true;
       });
    
  }

}
