import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from "./auth.service";

@Injectable({
    providedIn: 'root'
  })
export class JwtInterceptor implements HttpInterceptor {

    isAuthenticated : boolean;
    constructor(private authenticationService: AuthService) {
        this.authenticationService.isAuthenticated.subscribe(
            (isAuthenticated: boolean)  => this.isAuthenticated = isAuthenticated
          );
     }

     intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add auth header with jwt if user is logged in and request is to api url
        if (this.isAuthenticated) {
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${JSON.parse(this.authenticationService.getToken()).token}`
                }
            });
        }

        return next.handle(request);
    }
}