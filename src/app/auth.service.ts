import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Login } from './login-component/login';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  apiURL: string = 'http://localhost:8083/money-eligibility-service';

  public isAuthenticated = new BehaviorSubject<boolean>(false);

  constructor(private router: Router, private httpClient: HttpClient) {
  }

  async checkAuthenticated() {
    const authenticated = await localStorage.getItem("ACCESS_TOKEN");
    if(authenticated != null) {
      this.isAuthenticated.next(true);
      return true;
    }
    return false;
  }

   login(username: string, password: string) {
    let loginRequest = new Login(username, password);
    this.httpClient.post(this.apiURL + '/authenticate',loginRequest).subscribe(resp => {
      if (resp == null) {
        throw Error('We cannot handle the ' + 'FAILED' + ' status');
      }
      this.isAuthenticated.next(true);
      localStorage.setItem("ACCESS_TOKEN", JSON.stringify(resp));
      localStorage.setItem("USERNAME", username);
      this.router.navigate(['/home']);
    });
  }

  async logout(redirect: string) {
    try {
      localStorage.removeItem("ACCESS_TOKEN");
      localStorage.removeItem("USERNAME");
      this.isAuthenticated.next(false);
      this.router.navigate([redirect]);
    } catch (err) {
      console.error(err);
    }
  }
  
  getToken() {
    return localStorage.getItem("ACCESS_TOKEN");
  }
}
