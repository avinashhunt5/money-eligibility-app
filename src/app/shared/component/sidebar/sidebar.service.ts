import { catchError, retry } from 'rxjs/internal/operators';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';

@Injectable({
    providedIn: 'root'
  })
export class SideBarService {

    constructor(private httpClient: HttpClient) {
    }


     getUser(username: string): Observable<any> {
        let requestUrl = 'http://localhost:8083/user/getUser?userId=';
        return this.httpClient.get<any>(requestUrl + username).pipe(
          retry(3), catchError(this.handleError<any>('getUser')));
      }

     private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
          console.error(error);
          this.log(`${operation} failed: ${error.message}`);
      
          return of(result as T);
        };
      }
      
      private log(message: string) {
        console.log(message);
      }
  
}