import { Component, OnInit } from '@angular/core';
import { SideBarService } from './sidebar.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  firstName: string;
  lastName: string;
  constructor(private sidebarService: SideBarService) { }

  async ngOnInit() {
    let username = localStorage.getItem("USERNAME");
    this.sidebarService.getUser(username)
    .subscribe(data => {
      this.firstName = data.firstName;
      this.lastName = data.lastName;
    });
  }

}
