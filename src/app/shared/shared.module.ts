
import { NgModule } from '@angular/core';
import { FooterComponent } from './component/footer/footer.component';
import { SidebarComponent } from './component/sidebar/sidebar.component';
import { MaterialModule } from  '../material.module';
@NgModule({
  declarations: [
    FooterComponent,
    SidebarComponent,
  ],
  imports: [
    MaterialModule,
  ],
  exports: [
    FooterComponent,
    SidebarComponent
  ]
  
})
export class SharedModule { }
