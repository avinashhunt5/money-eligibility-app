export class Login {

    username: string;
    password: string;

    constructor(user: string, pass: string) {
        this.password = pass;
        this.username = user;
    }

}